import matplotlib.pyplot as plt
import numpy as np
xs = [0.01,0.05,0.1,0.2,0.25,0.4,0.5,1.0]


"""av action
#fill in these with data
mcmcs = [0.4999,0.5002,0.5008,0.4991,0.4986,0.4999,0.4994,0.4993]
ecmcs = [0.4996, 0.5001, 0.4987, 0.5003, 0.5002, 0.4987, 0.4994, 0.4993]
expected = [0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5]
mcmcerrors = [8.00E-04,7.31E-04,7.08E-04,6.64E-04,7.81E-04,6.78E-04,7.07E-04,6.64E-04]
ecmcerrors = [7.50E-04,6.54E-04,7.00E-04,6.91E-04,6.74E-04,6.80E-04,7.10E-04,7.87E-04]
observable = 'Average Action per site'
"""
"""obs1
mcmcs = [0.4996,0.4993,0.4977,0.4883,0.4832,0.4666,0.4524,0.3726]
ecmcs =[0.4991,0.4989,0.4954,0.4897,0.4849,0.4656,0.4524,0.3725]
expected = [0.49948,0.498773319,0.4966792,0.489395074,0.484584,0.466805,0.452893088,0.372975081]
mcmcerrors = [7.98E-04,7.28E-04,7.01E-04,6.45E-04,7.51E-04,6.31E-04,6.58E-04,4.98E-04]
ecmcerrors = [7.35E-04,6.54E-04,7.00E-04,6.79E-04,6.56E-04,6.42E-04,6.48E-04,6.10E-04]
observable = 'Observable 1'
"""
"""obs 2
mcmcs = [2.5121,0.3816,0.3165,0.2679,0.2468,0.208,0.1876,0.1268]
ecmcs = [3.6523,0.4806,0.333,0.2671,0.2457,0.2073,0.1881,0.1269]
expected = [5.182808559,0.49067,0.332075,0.265123198,0.246650155,0.20746875,0.18842764,0.12702492]
mcmcerrors = [0.8148,0.0245,0.008,0.0024,0.0014,5.62E-04,4.09E-04,2.09E-04]
ecmcerrors = [0.143,0.0093,0.0026,0.0011,7.57E-04,4.95E-04,4.00E-04,2.19E-04]
observable = 'Observable 2'
"""
"""ac action
mcmcs = [0.6169,0.5297,0.4865,0.4741,0.606,0.5127,0.5017,0.4824]
ecmcs = [0.5177,0.4482,0.4976,0.4829,0.4898,0.477,0.4893,0.5175]
expected = []
mcmcerrors = [0.0668,0.0473,0.0311,0.0524,0.0655,0.0459,0.0451,0.0309]
ecmcerrors = [0.0463,0.0292,0.0317,0.031,0.0313,0.0431,0.0313,0.0666]
observable = 'Autocorrelation time of Average Action'
"""
"""Ac obs 1
mcmcs = [0.6186,0.5277,0.4827,0.4652,0.5953,0.5043,0.519,0.4688]
ecmcs = [0.5172,0.4487,0.4998,0.4874,0.4905,0.4823,0.4883,0.5401]
expected = []
mcmcerrors = [0.0667,0.0471,0.0309,0.0515,0.0645,0.0453,0.0464,0.0303]
ecmcerrors = [0.0463,0.0292,0.0318,0.0312,0.0313,0.0435,0.0312,0.0799]
observable = 'Autocorrelation time of Observable 1'
"""

mcmcs = [96.7682,17.2331,7.4515,2.3718,1.5416,0.6113,0.5141,0.5011]
ecmcs = [0.4817,0.5951,0.4727,0.5192,0.4862,0.4947,0.4835,0.5422]
expected = []
observable = 'Integrated Autocorrelation time of Observable 2'
mcmcerrors = [37.7832,6.7762,2.2739,0.4798,0.2764,0.0766,0.0461,0.045]
ecmcerrors = [0.0309,0.0645,0.0428,0.0464,0.0311,0.0315,0.031,0.0593]

if len(mcmcs)==len(ecmcs):
	title = 'Plot of ' + observable + ' for 10000 sweeps (1000 Measurements)'
	plt.errorbar(xs[1:], mcmcs[1:], yerr=mcmcerrors[1:], color='red', label='Metropolis')
	plt.errorbar(xs[1:], ecmcs[1:], yerr=ecmcerrors[1:], color='blue', label='EventChain')
	if expected != []:
		plt.plot(xs[1:], expected[1:], color='black', label='Expected Numerically')
	plt.xlabel('Mass')
	plt.ylabel(observable)
	plt.legend()
	plt.ylim(0.2,10.0)
	plt.show()
	
