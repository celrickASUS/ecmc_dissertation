import numpy as np
d = 3.0
m = 0.5
k = 1.0/(m**2+2*d)
#print(k)
L = 128.0
#print(1-2*d*k)
S = []
momenta = []


for i in range(int(L)):
	S.append(i*2*np.pi/L)

for i in range(int(L)):
	for j in range(int(L)):
			for kh in range(int(L)):
				momenta.append([S[i],S[j],S[kh]])

#print(momenta)

chi = 0.

#works up to here
def g(mom,dire):
	return np.exp(-1.0j*mom[dire]) / (4.0*k*(np.sin(mom[0]/2.0)**2 + np.sin(mom[1]/2.0)**2 + np.sin(mom[2]/2)**2)+ 1.0 - 2*d*k)
counter = 0


for direction in range(int(d)):
	for momentum in range(len(momenta)):
		chi += g(momenta[momentum],direction)
		#print(g(momenta[momentum],direction))
		counter +=1
#print(counter)
#print(chi) #does not work here
chi = -2*k*chi*(1.0/(d*L**d))
#print(chi)


O_2 = 0
def f(x,y,z):
	return 1.0/(4*k*(np.sin(x/2.0)**2+np.sin(y/2.0)**2+np.sin(z/2.0)**2)+1-2*d*k)

for x in range(int(L)):
	for y in range(int(L)):
		for z in range(int(L)):
			O_2 += f(S[x],S[y],S[z])

obs_2 = 2*k*O_2/(4*L**3)
print(obs_2)
print(obs_2/(2*k))


O_1 = 4*obs_2 +chi
print(O_1/(2*k))
O_3 = 1.0/(L**d)*(k/(2-4*d*k)-obs_2)
print(O_3/(2*k))
 
