

lambda_0 = 0.5


phi = 4
sigma = -1.0



import numpy as np
a = -4.0*lambda_0
b = -12.0*lambda_0*sigma*phi
c= -2 + 4*lambda_0 - 12*lambda_0*(phi**2)
d = -2*sigma*phi - 4*lambda_0*sigma*(phi**3) + 4*lambda_0*sigma*phi
poly = np.poly1d([a,b,c,d])
print(np.roots(poly))
