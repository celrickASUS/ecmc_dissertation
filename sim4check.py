import phi4
import matplotlib.pyplot as plt
import numpy as np
import sys

def update_progress(progress):
                    barLength = 50 # Modify this to change the length of the progress bar
                    status = ""
                    if isinstance(progress, int):
                        progress = float(progress)
                    if not isinstance(progress, float):
                        progress = 0
                        status = "error: progress var must be float\r\n"
                    if progress < 0:
                        progress = 0
                        status = "Halt...\r\n"
                    if progress >= 1:
                        progress = 1
                        status = "Done...\r\n"
                    block = int(round(barLength*progress))
                    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block +\
                                           "-"*(barLength-block), progress*100, status)
                    sys.stdout.write(text)
                    sys.stdout.flush()
                    
                    
                    
                    
                    
test_values = [0.49,0.4,0.3,0.25,0.2,0.1,0.01]
steps = int(500)

for lambda_val in test_values:
    average_actions = []
    action_errors = []
    obs_1_values = []
    obs_1_errors = []
    labels = []
    average_actions_2 = []
    action_errors_2 = []
    obs_1_values_2 = []
    obs_1_errors_2 = []
    labels_2 = []
    labels.append("ECMClambda = " +str(lambda_val))
    labels_2.append("MCMClambda = " +str(lambda_val))
    for kappa_val in test_values:

        lattice_ecmc = phi4.phi4(2,32,kappa = kappa_val,lamda = lambda_val)

        
        ones = []
        total_actions = []

        for i in range(steps):
                    
            #update_progress(float(i)/float(steps))
            lattice_ecmc.ecmc_move()

            if i % 2 == 0:
                ones.append(lattice_ecmc.obs_1())
                total_actions.append(lattice_ecmc.total_action())

        observables = np.array([ones,total_actions])


        def std(s):
            return np.std(s)/np.sqrt(len(s))

        #Number of steps to use when figuring out averages
        sta= -50
        av_1, std_1= np.average(ones[sta:]), std(ones[sta:])
        av_A, std_A =np.average(total_actions[sta:]), std(total_actions[sta:])
        average_actions.append(av_A)
        action_errors.append(std_A)
        obs_1_values.append(av_1)
        obs_1_errors.append(std_1)
        #labels.append("ECMC kappa="+str(lattice_ecmc.kappa) +" lambda=" +str(lattice_ecmc.lambda_0))
        
        print("[O]The average action per site is % 5.5f +/ % 5.5f \n" %(av_A,std_A))

        print("[O]The average observable 1 is % 5.5f +/ % 5.5f \n" %(av_1,std_1))


        print("\n\n\n [+] ECMC")
        
    for kappa_val in test_values:
        lattice_mcmc = phi4.phi4(2,32,kappa = kappa_val,lamda = lambda_val)

        
        ones = []
        total_actions = []

        for i in range(steps):
            #update_progress(float(i)/float(steps))
            lattice_ecmc.mcmc_move()

            if i % 2 == 0:
                ones.append(lattice_ecmc.obs_1())
                total_actions.append(lattice_ecmc.total_action())

        observables = np.array([ones,total_actions])


        def std(s):
            return np.std(s)/np.sqrt(len(s))

        #Number of steps to use when figuring out averages
        sta= -50
        av_1, std_1= np.average(ones[sta:]), std(ones[sta:])
        av_A, std_A =np.average(total_actions[sta:]), std(total_actions[sta:])
        average_actions_2.append(av_A)
        action_errors_2.append(std_A)
        obs_1_values_2.append(av_1)
        obs_1_errors_2.append(std_1)
        #labels_2.append("MCMC kappa="+str(lattice_mcmc.kappa) +" lambda=" +str(lattice_mcmc.lambda_0))
        
        print("[O]The average action per site is % 5.5f +/ % 5.5f \n" %(av_A,std_A))

        print("[O]The average observable 1 is % 5.5f +/ % 5.5f \n" %(av_1,std_1))


        
    print("ecmc \n")
    print(average_actions)
    print("mcmc \n")
    print(average_actions_2)
    plt.plot(test_values, average_actions, label=labels[0])
    plt.plot(test_values, average_actions_2, label=labels_2[0])
    plt.title("Average action vs kappa")
    plt.legend()
    plt.savefig(labels[0] +".png")
    plt.show()





