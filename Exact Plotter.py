import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import make_interp_spline
sim_values = [0.5279797973,0.4319749933,0.4172183219,0.4095950130, 0.4055223815, 0.4006179927,0.4004087535,0.4004087265,0.4004087248]
continuum = [0.4004087262 for i in range(len(sim_values))]
sizes = np.log([4,6,7,8,9,16,32,64,128])
print(sizes)
#plt.plot(sizes,sim_values)
#plt.plot(sizes,continuum,alpha=0.3)
#plt.ylim(0.4003,0.410)
#plt.show()
xnew = np.linspace(sizes.min(),sizes.max())
spl = make_interp_spline(sizes, sim_values,k=3)
power_smooth = spl(xnew)

plt.plot(xnew, power_smooth, label='Numerical evaluations')
plt.plot(sizes,continuum,alpha=0.3, label ='Limit in infinite lattice')
plt.xlabel(r'$\log(L)$')
plt.ylabel(r'$\langle O_2 \rangle$')
plt.legend()
plt.show()
