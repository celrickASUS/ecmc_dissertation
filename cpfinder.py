import phi4
import matplotlib.pyplot as plt
import numpy as np
import sys

steps = int(1000)
runners = 5
masses = np.linspace(0.0,50.0,50)
values = []
for mass in masses:
	averager = 0.
	for run in range(runners):
		newobs = []
		lattice = phi4.phi4(2,8,mass=mass,lamda = 100)
		for step in range(steps):
			lattice.ecmc_move()
			newobs.append(np.absolute(lattice.newobs()))
		print("mass = " +str(mass) +", attempt " +str(run))
		print(np.average(newobs))
		averager += np.average(newobs)
		#plt.plot(np.arange(steps), newobs)
		#plt.show()
	values.append(averager/runners)

plt.plot(masses,values)
plt.show()


