"""
Gaussian Random Number generator

Created 27/05/2019
CE
"""

import numpy.random as random
import numpy as np
import matplotlib.pyplot as plot

x = [i for i in range(50000)]
y = [random.normal(scale=0.5) for j in range(512*512)]
mean = round(np.mean(y), 5)
stddev = round(np.std(y), 5)


str1 = "Mean = " +str(mean) + " \n and std dev = " + str(stddev)
plot.hist(y, bins=100)
plot.hist(y, bins=250)
plot.hist(y, bins=500)
plot.hist(y, bins=1000)
plot.text(2, 1500, str1)
plot.show()

