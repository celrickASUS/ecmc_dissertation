"""
Phi^4 THEORY in n dimensions.

This is the main class for ecmc simulation of Phi-4, containing all the 
used methods. 

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.


Created: 04/07/10
Created by Conor Elrick
"""
import fft
import numpy as np
import matplotlib.pyplot as plt
import cmath
np.seterr(all='raise')

class phi4(fft.FFT):
    """
     
    """
    
    def __init__(self, n, L, start_type = 'hot', kappa = 0.1, lamda = 0.3,\
                 multihit = 3,  mass = 1.):
        self.size = (n,L)
        self.log_file = open("logfile_phi4.out", 'w')
        self.system = self.system_setup(start_type)
        self.sigma = np.random.choice([-1,1]) # direction of ecmc movement
        self.chain_length = L**2 #event chain length
        self.lifted_site = 0
        self.acceptance = 0.
        self.multihit = multihit
        self.kappa = kappa
        self.lambda_0 = lamda
        
        try:
            self.omega = np.sqrt(2*self.lambda_0*(2*self.lambda_0 -1)) /2.0*self.lambda_0
        except:
            self.omega = None
            print("[O] Omega is complex. omega -> None")
        
        #self.mass = 0.5
        
    def action_self(self, site_value):
        """
        Self-interaction term in action. Note the differing normalisation to FFT
        """
       
        return site_value**2 + self.lambda_0*(site_value**2-1.0)**2
        #return 0.5*(self.mass**2)*(site_value**2) + self.lambda_0*(site_value**4)/24.0
        
    def action_interaction(self, site_value, neighbour_value):
        """
        Interaction term in the action. Note the differing normalisation to FFT.
        """
        
        return  -2.0*self.kappa*(site_value*neighbour_value)
        #return  0.5*((site_value - neighbour_value)**2)
    
    def ecmc_t_calc(self, energy, phi):
        """
        Case B
        """
        if self.lambda_0 >=0 and self.lambda_0 < 0.5:
            psi_2 = - self.sigma*phi
            if psi_2 <= 0:
                return self.ecmc_0_solve(energy,phi)
            else:
                return self.ecmc_1_solve(energy,phi,psi_2)

        else:
            pass
            print("E")
            return 2.0
            
    def ecmc_t_calc_neighbour(self, energy, phi_neighbour):
        if self.kappa*self.sigma*phi_neighbour <0:
            
            return energy/(-2*self.kappa*self.sigma*phi_neighbour)
        else :
            return 100000

    def ecmc_move(self):
        """
        Main loop for a chain of events
        """
        move_var = True
        chain_length = self.chain_length
        self.lifted_site = np.random.randint(0, self.system.size)
        while (move_var == True):
            
            neighbours = self.nearest_neighbours(self.lifted_site)

            
            moves = np.zeros([len(neighbours) + 1,2]) # +1 for self interaction

            
            #Self interaction
            delta_E = self.ecmc_energy()
            phi = self.system.flat[self.lifted_site]
            moves[0] = np.array([self.lifted_site, self.ecmc_t_calc(delta_E,\
                                phi)])

            
            #Pair interaction
            i = 1
            for neighbour in neighbours:
                phi_n = self.system.flat[neighbour]
                delta_E = self.ecmc_energy()

                moves[i] = np.array([neighbour, self.ecmc_t_calc_neighbour(delta_E,\
                                    phi_n)])
                i += 1
            t_move = moves[np.argmin(np.transpose(moves)[1], axis=0)]

            
            if t_move[1] < self.chain_length:
                
                
                    
                self.chain_length -= t_move[1]
                
                self.system.flat[self.lifted_site] += self.sigma * t_move[1]
                    
                if t_move[0] == self.lifted_site:    
                
                    self.sigma *= -1

                self.lifted_site = int(t_move[0])

            else:
                self.system.flat[self.lifted_site] += self.sigma *\
                                                (self.chain_length)
                if t_move[0] == self.lifted_site:
                    self.sigma *= -1

                move_var = False
                self.chain_length = chain_length

           
        return None
    
    def ecmc_0_solve(self, energy, phi):
        lam = self.lambda_0
        sols = np.roots([+lam, +4.0*self.sigma*lam*phi, +1-2*lam+6*lam*phi**2, +self.sigma*(2-4*lam)*phi+4*self.sigma*lam*phi**3, -energy])
        sols = sols.real[abs(sols.imag)<1e-5]
        sols = sols[sols>0]
        #print(len(sols))
        return min(sols)

    def ecmc_1_solve(self, energy, phi, w):
        lam = self.lambda_0
        sols = np.roots([+lam, +4.0*self.sigma*lam*phi, +1-2*lam+6*lam*phi**2, +self.sigma*(2-4*lam)*phi+4*self.sigma*lam*phi**3, -energy -lam*w**4 -4.0*self.sigma*lam*phi*w**3 -(1-2*lam+6*lam*phi**2)*w**2 -self.sigma*(2-4*lam)*phi*w -4.0*self.sigma*lam*phi**3*w])
        #print(sols)
        sols = sols.real[abs(sols.imag)<1e-5]
        sols = sols[sols>0]
        sols = sols[sols> w]
        #print(len(sols))
        if len(sols) > 1:
            raise SOLError
        return min(sols)
        
    def mc_local_step(self,site):
        """
        MCMC move given a site and an action. The neighbours and self are
        split in two cases.
        """
        
        old_site_value = self.system.flat[site]
        new_site_value = old_site_value + np.random.normal(loc=0.,scale=1.5)
        
        #True until proven otherwise
        move_acceptance = True
        
        old_action = 0.
        new_action = 0.
        
        #nearest neighbour term
        for neighbour in self.nearest_neighbours(site):
            
            old_action += self.action_interaction(old_site_value,\
                                                 self.system.flat[neighbour])
            
            new_action += self.action_interaction(new_site_value,\
                                                 self.system.flat[neighbour])
            
        #self term
        old_action += self.action_self(old_site_value)
        new_action += self.action_self(new_site_value)
        
        #Metropolis Step
        try:
            P_acceptance = min([1, np.exp(old_action - new_action)])
        except FloatingPointError as error:
            #print(error)
            P_acceptance = 0.

        gamma = np.random.uniform(0,1)
        
        if P_acceptance <= gamma:
            move_acceptance = False
        
        #Acceptance
        if move_acceptance == True:
            self.acceptance += 1.
            self.system.flat[site] = new_site_value
           
        
        return None
       
    
        
        
if __name__ == '__main__':
    lattice = phi4(2,2, lamda = 0.1)

    steps = int(50)

    for i in range(steps):

        lattice.mcmc_move()
    pass
