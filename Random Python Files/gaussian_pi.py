import numpy as np
import matplotlib.pyplot as plt

def main(n_moves, width):
    accepted_moves = 0.
    for move in range(n_moves):
        x,y = get_pair(width)
        if gaussian(x) > y:
            accepted_moves += 1
    return accepted_moves
    
def main(n_moves,
    
def gaussian(x):
    return np.exp(-1.0*(float(x)**2))
    
def get_pair(width):
    return np.random.uniform(-width,width), np.random.uniform(0,1)
    
def pi(N_moves,r):
    return (main(N_moves,r)/N_moves*2*r)**2
    
    
if __name__ == '__main__':
    N_moves = 10000000
    r = 3
    
    accs = []
    t = 0
    for i in range(N_moves):
		t +=main(1,r)
		accs.append(t)
    total_moves = [i+1 for i in range(N_moves)]
    
    pis_0 = [accs[i]/total_moves[i]*2*r for i in range(len(accs))]
    pis = np.square(pis_0)
    
    plt.plot(total_moves[1000:], pis[1000:])
    plt.show()
    
    
