import numpy as np
import matplotlib.pyplot as plt

MASS = 0.5
NSTEPS = 500000
actions = []
acceptance = 0

def action(phi):
    return 0.5*MASS**2*(phi**2)

lattice = np.array([0.])

#setup lattice
lattice[0] = np.random.normal(loc=0,scale=5)

for step in range(NSTEPS):
    phi_old = lattice[0]
    phi_new = phi_old + np.random.normal(loc=0,scale=0.5)
    
    #phi_new = phi_old+ np.random.choice([1.,-1.])*0.1
    old_action = action(phi_old)
    new_action = action(phi_new)
    
    P_accept = min([1,np.exp(old_action - new_action)])
    gamma = np.random.uniform(0,1)
    if gamma < P_accept:
        lattice[0] = phi_new
        acceptance += 1
    else:
        pass
    actions.append(action(lattice[0]))


actions = actions[NSTEPS/10:]
print("action is "+ str(np.average(actions))+ "with dev " + str(np.std(actions)/np.sqrt(len(actions))))
print("acceptance " + str(100.*acceptance/NSTEPS))

plt.plot([x for x in range(len(actions))],actions)
plt.show()


#print(actions)
