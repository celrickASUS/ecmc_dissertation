import numpy as np
import matplotlib.pyplot as plt


start = 1
end = 7
xs= np.log([0.1,1.0,10.0,50.0,100.0,500.0,1000.0])


mcmcs = [11.0586,1.2071,0.1427, 0.0823, 0.0652,0.0520,0.0663]
mcmcerror = [0.0258,0.0048,0.0006,0.0007,0.0019,0.0168,0.0377]
ecmcs = [14.6527,1.2125,0.1429,0.0823,0.0636,0.0,0.0243]
ecmcerror = [0.0908,0.0016,0.0008,0.0008,0.0007,0.0,0.0006]

Observable = 'Observable 2'
title = Observable + r'vs $lambda$' 

"""
mcmcs = [2.3289,3.2236,0.5003,0.6781,2.1924,32.8054,71.9063]
mcmcerror = [0.4020,0.6290,0.0258,0.0685,0.3635,13.6259,34.1523]
ecmcs = [13.5009,0.5486,0.5155,0.5036,0.4990,0.5183,0.5034]
ecmcerror = [4.3539,0.0487,0.0375,0.0367,0.0364,0.5067,0.0367]

Observable = 'Integrated AC time of Observable 2'
title = Observable + r'vs $lambda$' 
"""


plt.errorbar(xs[start:end], mcmcs[start:end], yerr=mcmcerror[start:end], marker ='x',ls= 'None',color='red',\
             label='Metropolis')
plt.errorbar(xs[start:end], ecmcs[start:end], yerr=ecmcerror[start:end], marker='x',ls='None',color='blue',\
             label='Event-chain')
plt.xlabel(r'$\log(\lambda$)')
plt.ylabel(Observable)
plt.legend()
plt.title(title)




from scipy.interpolate import make_interp_spline, BSpline

xnew = np.linspace(xs[start:end].min(),xs[start:end].max(),300) #300 represents number of points to make between T.min and T.max

spl = make_interp_spline(xs[start:end], mcmcs[start:end], k=3) #BSpline object
power_smooth = spl(xnew)
spl2 = make_interp_spline(xs[start:end], ecmcs[start:end], k=3) #BSpline object
power_smooth2 = spl2(xnew)
plt.plot(xnew,power_smooth,color ='red')
plt.plot(xnew,power_smooth2,color='blue')
plt.show()
