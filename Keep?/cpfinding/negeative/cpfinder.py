import phi4
import matplotlib.pyplot as plt
import numpy as np
import sys

steps = int(5000)
runners = 5
masses = np.linspace(0.0,4.0,40)
values = []
for mass in masses:
	averager = 0.
	for run in range(runners):
		newobs = []
		lattice = phi4.phi4(2,10,mass=mass*1.0j,lamda = 133.2)
		for step in range(steps):
			lattice.mcmc_move()
			newobs.append(np.absolute(lattice.newobs()))
		print("mass = " +str(mass) +", attempt " +str(run))
		print(np.average(newobs))
		averager += np.average(newobs)
		#plt.plot(np.arange(steps), newobs)
		#plt.show()
	values.append(averager/runners)

plt.plot([-mass**2 for mass in masses],values)
plt.xlabel(r'$m^2$')
plt.ylabel('Average magnetisation')
plt.savefig("negative.png")
plt.clf()

print(masses)
print(values)

