"""
Phi^4 THEORY in n dimensions.

This is the main class for ecmc simulation of Phi-4, containing all the 
used methods. 

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.


Created: 04/07/10
Created by Conor Elrick
"""
import fft
import numpy as np
import matplotlib.pyplot as plt

class phi4(fft.FFT):
    """
     
    """
    
    def __init__(self, n, L, start_type = 'hot', kappa = 0.1, lamda = 0.3,\
                 multihit = 3,  mass = 1.):
        self.size = (n,L)
        self.log_file = open("logfile_phi4.out", 'w')
        self.system = self.system_setup(start_type)
        self.sigma = np.random.choice([-1,1]) # direction of ecmc movement
        self.chain_length = L**2 #event chain length
        self.lifted_site = 0
        self.acceptance = 0.
        self.multihit = multihit
        self.kappa = kappa
        self.lambda_0 = lamda
        
        self.mass = 0.5
        
    def action_self(self, site_value):
        """
        Self-interaction term in action. Note the differing normalisation to FFT
        """
       
        return site_value**2 + self.lambda_0*(site_value**2-1.0)**2
        #return 0.5*(self.mass**2)*(site_value**2) + self.lambda_0*(site_value**4)/24.0
        
    def action_interaction(self, site_value, neighbour_value):
        """
        Interaction term in the action. Note the differing normalisation to FFT.
        """
        
        return  -2.0*self.kappa*(site_value*neighbour_value)
        #return  0.5*((site_value - neighbour_value)**2)
    
    def ecmc_t_calc(self, energy, delta_phi):
        pass
        return None
        
    def mc_local_step(self,site):
        """
        MCMC move given a site and an action. The neighbours and self are
        split in two cases.
        """
        
        old_site_value = self.system.flat[site]
        new_site_value = old_site_value + np.random.normal(loc=0.,scale=1.5)
        
        #True until proven otherwise
        move_acceptance = True
        
        old_action = 0.
        new_action = 0.
        
        #nearest neighbour term
        for neighbour in self.nearest_neighbours(site):
            
            old_action += self.action_interaction(old_site_value,\
                                                 self.system.flat[neighbour])
            
            new_action += self.action_interaction(new_site_value,\
                                                 self.system.flat[neighbour])
            
        #self term
        old_action += self.action_self(old_site_value)
        new_action += self.action_self(new_site_value)
        
        #Metropolis Step
        P_acceptance = min([1, np.exp(old_action - new_action)])
        gamma = np.random.uniform(0,1)
        
        if P_acceptance <= gamma:
            move_acceptance = False
        
        #Acceptance
        if move_acceptance == True:
            self.acceptance += 1.
            self.system.flat[site] = new_site_value
           
        
        return None
       
    
        
        
if __name__ == '__main__':
    
    pass
