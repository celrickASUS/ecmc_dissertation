import phi4
import matplotlib.pyplot as plt
import numpy as np
import sys



lattice = phi4.phi4(2,32,kappa = float(sys.argv[1]),lamda = 0.01)

steps = int(5000)
ones = []
twos = []
threes = []
fours = []
total_actions = []
print(lattice.system)

def update_progress(progress):
            barLength = 50 # Modify this to change the length of the progress bar
            status = ""
            if isinstance(progress, int):
                progress = float(progress)
            if not isinstance(progress, float):
                progress = 0
                status = "error: progress var must be float\r\n"
            if progress < 0:
                progress = 0
                status = "Halt...\r\n"
            if progress >= 1:
                progress = 1
                status = "Done...\r\n"
            block = int(round(barLength*progress))
            text = "\rPercent: [{0}] {1}% {2}".format( "#"*block +\
                                   "-"*(barLength-block), progress*100, status)
            sys.stdout.write(text)
            sys.stdout.flush()
            


for i in range(steps):
            
    update_progress(float(i)/float(steps))
    lattice.mcmc_move()

    if i % 10 == 0:
        ones.append(lattice.obs_1())
        twos.append(lattice.obs_2())
        threes.append(lattice.obs_3())
        fours.append(lattice.obs_4())
        total_actions.append(lattice.total_action())
print(len(total_actions))

observables = np.array([ones,twos,threes,total_actions])
#print(observables)
np.savetxt(str(lattice.kappa) + str(lattice.lambda_0) +'observables.txt',observables.transpose())
print(lattice.system)
"""
#Number of steps to use when figuring out averages
sta= -5
av_1, std_1= np.average(ones[sta:]), std(ones[sta:])
av_2, std_2= np.average(twos[sta:]), std(twos[sta:])
av_3, std_3= np.average(threes[sta:]), std(threes[sta:])
av_4, std_4= np.average(fours[sta:]), std(fours[sta:])
av_A, std_A =np.average(total_actions[sta:]), std(total_actions[sta:])

print("[O]The average action per site is % 5.5f +/ % 5.5f \n" %(av_A,std_A))
print("boogiewoo")

print("[O]The average observable 1 is % 5.5f +/ % 5.5f \n" %(av_1,std_1))
print("[O]The average observable 2 is % 5.5f +/ % 5.5f \n" %(av_2,std_2))
print("[O]The average observable 3 is % 5.5f +/ % 5.5f \n" %(av_3,std_3))
print("[O]The average observable 4 is % 5.5f +/ % 5.5f \n" %(av_4,std_4))

#Plotting of Results
xvalues = np.arange(0,len(twos))

plt.plot(xvalues, threes)
plt.show()


plt.plot(xvalues, total_actions)
plt.show()

print("\n\n\n [+] ECMC")


"""
Acceptance = 100.*lattice.acceptance/(steps*lattice.system.size*lattice.multihit)
print("\n\n")
print("[+] Simulation settings \n L = % d \n d = % d \n lambda = % 5.4f \n kappa = % 5.4f" %(lattice.size[1], lattice.size[0], lattice.lambda_0, lattice.kappa))
                                                           
print("[+] Moves \n Number of moves %d \n Acceptance = % 5.2f" %(steps,\
                                                                 Acceptance ))

