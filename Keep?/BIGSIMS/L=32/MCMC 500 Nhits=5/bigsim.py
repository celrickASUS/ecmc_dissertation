import fft 
import matplotlib.pyplot as plt
import numpy as np
import sys



def update_progress(progress):
            barLength = 10 # Modify this to change the length of the progress bar
            status = ""
            if isinstance(progress, int):
                progress = float(progress)
            if not isinstance(progress, float):
                progress = 0
                status = "error: progress var must be float\r\n"
            if progress < 0:
                progress = 0
                status = "Halt...\r\n"
            if progress >= 1:
                progress = 1
                status = "Done...\r\n"
            block = int(round(barLength*progress))
            text = "\rPercent: [{0}] {1}% {2}".format( "#"*block +\
                                   "-"*(barLength-block), progress*100, status)
            sys.stdout.write(text)
            sys.stdout.flush()
                
                
masses = [0.01,0.1,0.25,0.5,1.0,2.0]
for mass in masses:
    lattice = fft.FFT(2,32, mass=mass, multihit=5)
    steps = 500

    ones = []
    twos = []
    threes = []
    fours = []
    total_actions = []


                



    for i in range(steps):
                
        update_progress(float(i)/float(steps))
        lattice.mcmc_move()

        if i % 10 == 0:
            ones.append(lattice.obs_1())
            twos.append(lattice.obs_2())
            threes.append(lattice.obs_3())
            fours.append(lattice.obs_4())
            total_actions.append(lattice.total_action())
    print(len(total_actions))

    observables = np.array([ones,twos,threes,fours, total_actions])

    np.savetxt(str(mass)+' observables.txt', observables.transpose())
    
    Acceptance = 100.*lattice.acceptance/(steps*lattice.system.size)
    print("\n\n")
    print("[+] Simulation settings \n L = % d \n d = % d \n m = % 5.4f" \
          %(lattice.size[1], lattice.size[0], lattice.mass))
                                                           
    print("[+] Moves \n Number of moves %d \n Acceptance = % 5.2f" %(steps,\
                                                                 Acceptance ))




