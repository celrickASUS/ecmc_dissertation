import fft
import numpy as np
import matplotlib.pyplot as plt
import cmath
np.seterr(all='raise')

class phi4(fft.FFT):

    def __init__(self, n, L, start_type = 'hot', lamda = 0.3,\
                 multihit = 3,  mass = 1):
        self.size = (n,L)
        self.system = self.system_setup(start_type)
        self.sigma = np.random.choice([-1,1]) # direction of ecmc movement
        self.chain_length = L**2 #event chain length
        self.lifted_site = 0

        self.lambda_0 = lamda
        self.mass = mass
        try:
            self.omega = np.sqrt(-6*self.mass**2/self.lambda_0)
        except:
            self.omega = None
            print("[O] Omega is complex. omega -> None")
        
    
    def ecmc_t_calc(self, energy, phi):
        """
        Case B
        """
        #if self.lambda_0 *self.mass**2 >0:
        if self.omega == None:
            #Only one root
            psi_2 = - self.sigma*phi
            if psi_2 <= 0:
                return self.ecmc_0_solve(energy,phi)
            else:
                return self.ecmc_1_solve(energy,phi,psi_2)
        else:
            # 3 roots
            psi_2 = -self.sigma*phi
            psi_1 = psi_2 - self.omega
            psi_3 = psi_2 + self.omega
            if psi_3 < 0:
                return self.ecmc_0_solve(energy,phi)
                
            elif psi_2 <0:
                return self.ecmc_2_solve(energy,phi)
            elif psi_1 <0:
                energy_subs = np.real(-(1.0/24)*self.lambda_0*phi**2+0.5*self.mass**2*phi**2)
                if energy_subs <= energy:
                    return self.ecmc_2_solve(energy-energy_subs,phi)
                else:
                    return self.ecmc_0_solve(energy,phi)
            elif psi_1 >0:
                energy_subs = np.real(-3.0*self.mass**4/(2.0*self.lambda_0))
                if energy_subs <= energy:
                    return self.ecmc_2_solve(energy-energy_subs,phi)
                else:
                    return self.ecmc_2_solve(energy,phi)
                    #same equation as case 2
            else:
                raise FloatingPointError
                
    


            
    def ecmc_t_calc_neighbour(self, energy, delta_phi,mass):

        if self.sigma * delta_phi >= 0:
            
            return np.sqrt((2.0 * energy / (mass**2)) + delta_phi**2 ) -\
                   self.sigma*delta_phi
        else:
            return np.sqrt(2.0 * energy / (mass**2)) -\
                   self.sigma*delta_phi

    def ecmc_move(self):
        """
        Main loop for a chain of events
        """
        move_var = True
        chain_length = self.chain_length
        self.lifted_site = np.random.randint(0, self.system.size)
        while (move_var == True):
            neighbours = self.nearest_neighbours(self.lifted_site)

            moves = np.zeros([len(neighbours) + 1,2]) # +1 for self interaction

            #Self interaction
            delta_E = self.ecmc_energy()
            phi = self.system.flat[self.lifted_site]
            moves[0] = np.array([self.lifted_site, self.ecmc_t_calc(delta_E,\
                                phi)])
            #Pair interaction
            i = 1
            #   #
            for neighbour in neighbours:
                delta_phi = self.system.flat[self.lifted_site] -\
                            self.system.flat[neighbour]
                
                delta_E = self.ecmc_energy()
                
                moves[i] = np.array([neighbour, self.ecmc_t_calc_neighbour(delta_E,\
                                    delta_phi, 1.0)])
                                 
                i += 1
                
            t_move = moves[np.argmin(np.transpose(moves)[1], axis=0)]
            #     #   
            if t_move[1] < self.chain_length:  
                self.chain_length -= t_move[1]
                
                self.system.flat[self.lifted_site] += self.sigma * t_move[1]
                    
                if t_move[0] == self.lifted_site:    
                
                    self.sigma *= -1

                self.lifted_site = int(t_move[0])

            else:
                self.system.flat[self.lifted_site] += self.sigma *\
                                                (self.chain_length)
                if t_move[0] == self.lifted_site:
                    self.sigma *= -1

                move_var = False
                self.chain_length = chain_length
        return None
    
    def ecmc_0_solve(self, energy, phi):
        L = self.lambda_0
        M = self.mass**2
        S = self.sigma
        return -S*phi + (1.0/L)*np.sqrt(-6*L*M +L*np.sqrt(L**2*phi**4 + 12*M*L*phi**2 + 36*M**2 + 24*energy*L))

    def ecmc_1_solve(self, energy, phi, w):
        L = self.lambda_0
        M = self.mass**2
        S = self.sigma
        return -S*phi + (1.0/L)*np.sqrt(-6*L*M +2*L*np.sqrt(9*M**2 + 6*energy*L))
        
    def ecmc_2_solve(self, energy, phi):
        L = self.lambda_0
        M = self.mass**2
        S = self.sigma
        return -S*phi + (1.0/L)*np.sqrt(2*L*np.sqrt(6*energy*L)-6*L*M)
        
