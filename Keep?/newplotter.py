import numpy as np
import matplotlib.pyplot as plt
xs = np.log([4,16,32,64,128])
ys_1 = [0.3823529417,0.10649360340,0.05325920944,0.02662960697,0.01331480350]
ys_2 = [0.2326262622,0.01503703905,0.00375936644,0.00093984162,0.00023496040]
ys_3 = [0.0915980305,0.00144532536,0.00018066622,0.00002258328,0.00000282291]

plt.plot(xs,ys_1,color='red',label=r'd=1')
plt.plot(xs,ys_2,color='blue',label=r'd=2')
plt.plot(xs,ys_3,color='green',label=r'd=3')
plt.legend()
plt.xlabel('Lattice size, '+r'$L$')
plt.ylabel(r'$\langle O_3 \rangle$')
plt.show()

from scipy.interpolate import make_interp_spline, BSpline

xnew = np.linspace(xs.min(),xs.max(),300) #300 represents number of points to make between T.min and T.max

spl = make_interp_spline(xs, ys_1, k=3) #BSpline object
power_smooth = spl(xnew)


spl_2 = make_interp_spline(xs, ys_2, k=3) #BSpline object
power_smooth_2 = spl_2(xnew)

spl_3 = make_interp_spline(xs, ys_3, k=3) #BSpline object
power_smooth_3 = spl_3(xnew)

plt.plot(xnew,power_smooth,color='red',label=r'd=1')
plt.plot(xnew,power_smooth_2,color='blue',label=r'd=2')
plt.plot(xnew,power_smooth_3,color='green',label=r'd=3')
plt.legend()
plt.xlabel('Lattice size, '+r'$\log(L)$')
plt.ylabel(r'$\langle O_3 \rangle$')
plt.show()
